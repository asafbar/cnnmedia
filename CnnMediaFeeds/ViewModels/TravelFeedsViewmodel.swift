//
//  TravelFeedsViewmodel.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 22/10/2020.
//

import Foundation

final class TravelFeedsViewmodel {
    
    var timer: Timer?
    var travelFeeds = Observable(FeedModel())
    
    var lastBuildDate: Date? {
        didSet {
            if let newDate = lastBuildDate,
               let lastDate = travelFeeds.value.lastBuildDate,
               newDate > lastDate {
                print("Fetch Travel again")
                self.fetcTravelFeeds()
            } else {
                print("No need to fetch new data on Travel")
            }
        }
    }

    func activateAutoUpdate() {
        createUpdatingTimer()
    }
    
    func deActivateAutoUpdate() {
        timer?.invalidate()
    }
    
    private func createUpdatingTimer() {
        timer = Timer.scheduledTimer(timeInterval: Config.shared.updateInterval, target: self, selector: #selector(fetchLastDateBuild), userInfo: nil, repeats: true)
    }
    
    func fetcTravelFeeds() {
        if let travelUrl = URL(string: Config.RssUrlString.travel.rawValue) {
            Config.shared.networkManager?.fetcData(url: travelUrl, timeout: 12, completion: { [weak self] result in
                
                switch result {
                case .success(let data):
                    do {
                        if let data = data {
                            let parser = FeedsXMLParser(data: data)
                            let res: (feed: FeedModel?, error: NSError?) = parser.parse()
                            
                            if let feed = res.feed {
                                print(feed)
                                self?.travelFeeds.value = feed
                            } else {
                                // handle error
                            }
                        }
                    }
                case .failure:
                    print("Error")
                    return
                }
            })
        }
    }
    
    @objc func fetchLastDateBuild() {
        print("Update Travel fired")
        if let travelUrl = URL(string: Config.RssUrlString.travel.rawValue) {
            Config.shared.networkManager?.fetcData(url: travelUrl, timeout: 5, completion: { [weak self] result in
                
                switch result {
                case .success(let data):
                    do {
                        if let data = data {
                            let parser = LastBuildDateXMLParser(data: data)
                            let res: (lastBuilldDate: Date?, error: NSError?) = parser.parse()
                            
                            print(res)
                            self?.lastBuildDate = res.lastBuilldDate
                        }
                    }
                case .failure:
                    print("Error")
                    return
                }
            })
        }
    }
}
