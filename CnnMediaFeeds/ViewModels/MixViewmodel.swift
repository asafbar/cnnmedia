//
//  MixViewmodel.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class FeedType {
    var name: String?
    var feed: FeedModel?
}

class MixViewmodel {
    
    enum DataType: String {
        case worldSport = "CNN.com - RSS Channel - Sport"
        case entertainment = "CNN.com - RSS Channel - Entertainment"
    }
    
    var sportTimer: Timer?
    var entertainmentTimer: Timer?
    var mixDataFeeds = Observable([FeedType()])
    
    var lastBuildDateSport: Date? {
        didSet {
            if let newDate = lastBuildDateSport {
                let feedTypes = mixDataFeeds.value
                let feedItem = feedTypes.filter({ $0.name == MixViewmodel.DataType.worldSport.rawValue }).first
                if let lastDate = feedItem?.feed?.lastBuildDate,
                   newDate > lastDate {
                    print("Fetch worldSport again")
                    if let index = mixDataFeeds.value.firstIndex(where: { $0.name == MixViewmodel.DataType.worldSport.rawValue }) {
                        mixDataFeeds.value.remove(at: index)
                    }
                    fetcFeeds(urlString: Config.RssUrlString.worldSport.rawValue)
                } else {
                    print("No need to fetch new data on worldSport")
                }
            }
        }
    }
    
    var lastBuildDateEntertainment: Date? {
        didSet {
            if let newDate = lastBuildDateEntertainment {
                let feedTypes = mixDataFeeds.value
                let feedItem = feedTypes.filter({ $0.name == MixViewmodel.DataType.entertainment.rawValue }).first
                if let lastDate = feedItem?.feed?.lastBuildDate,
                   newDate > lastDate {
                    print("Fetch Entertainment again")
                    if let index = mixDataFeeds.value.firstIndex(where: { $0.name == MixViewmodel.DataType.entertainment.rawValue }) {
                        mixDataFeeds.value.remove(at: index)
                    }
                    fetcFeeds(urlString: Config.RssUrlString.entertainment.rawValue)
                } else {
                    print("No need to fetch new data on Entertainment")
                }
            }
        }
    }
    
    func activateAutoUpdate() {
        createUpdatingTimer()
    }
    
    func deActivateAutoUpdate() {
        sportTimer?.invalidate()
        entertainmentTimer?.invalidate()
    }
    
    private func createUpdatingTimer() {
        sportTimer = Timer.scheduledTimer(timeInterval: Config.shared.updateInterval, target: self, selector: #selector(fetchSportLastDateBuild), userInfo: nil, repeats: true)
        entertainmentTimer = Timer.scheduledTimer(timeInterval: Config.shared.updateInterval, target: self, selector: #selector(fetchEntertainmentLastDateBuild), userInfo: nil, repeats: true)
    }
    
    func fetchData() {
        mixDataFeeds.value.removeAll()
        fetcFeeds(urlString: Config.RssUrlString.worldSport.rawValue)
        fetcFeeds(urlString: Config.RssUrlString.entertainment.rawValue)
    }
    
    func fetcFeeds(urlString: String) {
        if let travelUrl = URL(string: urlString) {
            Config.shared.networkManager?.fetcData(url: travelUrl, timeout: 12, completion: { [weak self] result in
                
                switch result {
                case .success(let data):
                    do {
                        if let data = data {
                            let parser = FeedsXMLParser(data: data)
                            
                            let res: (feed: FeedModel?, error: NSError?) = parser.parse()
                            
                            if let feed = res.feed {
                                print(feed)
                                let feedType = FeedType()
                                feedType.name = feed.title
                                feedType.feed = feed
                                self?.mixDataFeeds.value.append(feedType)
                            } else {
                                // handle error
                            }
                        }
                    }
                case .failure:
                    print("Error")
                    return
                }
            })
        }
    }
    
    @objc func fetchSportLastDateBuild() {
        print("Update worldSport fired")
        if let travelUrl = URL(string: Config.RssUrlString.worldSport.rawValue) {
            Config.shared.networkManager?.fetcData(url: travelUrl, timeout: 5, completion: { [weak self] result in
                
                switch result {
                case .success(let data):
                    do {
                        if let data = data {
                            let parser = LastBuildDateXMLParser(data: data)
                            let res: (lastBuilldDate: Date?, error: NSError?) = parser.parse()
                            
                            print(res)
                            self?.lastBuildDateSport = res.lastBuilldDate
                        }
                    }
                case .failure:
                    print("Error")
                    return
                }
            })
        }
    }
    
    @objc func fetchEntertainmentLastDateBuild() {
        print("Update Entertainment fired")
        if let travelUrl = URL(string: Config.RssUrlString.entertainment.rawValue) {
            Config.shared.networkManager?.fetcData(url: travelUrl, timeout: 5, completion: { [weak self] result in
                
                switch result {
                case .success(let data):
                    do {
                        if let data = data {
                            let parser = LastBuildDateXMLParser(data: data)
                            let res: (lastBuilldDate: Date?, error: NSError?) = parser.parse()
                            
                            print(res)
                            self?.lastBuildDateEntertainment = res.lastBuilldDate
                        }
                    }
                case .failure:
                    print("Error")
                    return
                }
            })
        }
    }

}
