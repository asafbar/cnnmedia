//
//  MainViewmodel.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 22/10/2020.
//

import Foundation

final class MainViewmodel {
    
    var timer: Timer?
    var date = Observable("")
    
    init() {
        createTimer()
    }
    
    private func createTimer() {
        timer = Timer.scheduledTimer(timeInterval: Config.shared.appDateUpdateInterval, target: self, selector: #selector(updateCurrentDateAndTime), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateCurrentDateAndTime() {
        date.value = Date().stringFormatted()
    }
    
}
