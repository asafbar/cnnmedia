//
//  MixDataVC.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import UIKit

class MixDataVC: UIViewController {

    let viewmodel = MixViewmodel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        
        viewmodel.fetchData()
    }
    
    private func setupView() {
        setupTableview()
        bindViewmodel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.navigationItem.title = "Mix data"
        
        viewmodel.activateAutoUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewmodel.deActivateAutoUpdate()
    }
    
    private func bindViewmodel() {
        viewmodel.mixDataFeeds.bind { [weak self] feedTypes in
            self?.tableView.reloadData()
        }
    }
    
    private func setupTableview() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MixDataCell.cellNib, forCellReuseIdentifier: MixDataCell.cellID)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webViewController" {
            if let cell = sender as? MixDataCell {
                if let webView = segue.destination as? WebViewController {
                    webView.urlString = cell.feed?.link
                }
            }
        }
    }
}

extension MixDataVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let feedTypes = viewmodel.mixDataFeeds.value

        if feedTypes.count == 1 {
            return feedTypes.first?.feed?.items.count ?? 0
        } else {
            switch section {
            case 0:
                return feedTypes.filter({ $0.name == MixViewmodel.DataType.worldSport.rawValue }).first?.feed?.items.count ?? 0
            case 1:
                return feedTypes.filter({ $0.name == MixViewmodel.DataType.entertainment.rawValue }).first?.feed?.items.count ?? 0
            default:
                return 0
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print("Number of sections: \(viewmodel.mixDataFeeds.value.count)")
        return viewmodel.mixDataFeeds.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MixDataCell.cellID, for: indexPath) as? MixDataCell else { return UITableViewCell() }
        
        let feedTypes = viewmodel.mixDataFeeds.value
        
        if feedTypes.count == 1 {
            if let feedItem = feedTypes.first?.feed?.items[indexPath.row] {
                cell.configure(feed: feedItem)
            }
            
        } else {
            switch indexPath.section {
            case 0:
                let feedType = feedTypes.filter({ $0.name == MixViewmodel.DataType.worldSport.rawValue }).first
                if let feedItem = feedType?.feed?.items[indexPath.row] {
                    cell.configure(feed: feedItem, colorType: MixDataCell.MixCellColorType.sport)
                }
            case 1:
                let feedType = feedTypes.filter({ $0.name == MixViewmodel.DataType.entertainment.rawValue }).first
                if let feedItem = feedType?.feed?.items[indexPath.row] {
                    cell.configure(feed: feedItem, colorType: MixDataCell.MixCellColorType.entertainment)
                }
            default:
                break
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? MixDataCell
        
        if let feed = cell?.feed {
            CnnMediaDefaults().setLastOppened(feed: feed)
        }
        
        performSegue(withIdentifier: "webViewController", sender: cell)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //CNN.com - RSS Channel -
        let feedTypes = viewmodel.mixDataFeeds.value
        return feedTypes[section].name?.replacingOccurrences(of: "CNN.com - RSS Channel - ", with: "")
    }
}
