//
//  MainVC.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 22/10/2020.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lastOpennedFeddLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    var viewmodel = MainViewmodel()
    var lastOppenedFeed: FeedItemModel? {
        didSet {
            lastOpennedFeddLabel.text = lastOppenedFeed?.title != "" ? lastOppenedFeed?.title : "No title"
            lastOpennedFeddLabel.isUserInteractionEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        viewmodel.updateCurrentDateAndTime()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let feed = CnnMediaDefaults().getLastOppenedFeed() {
            lastOppenedFeed = feed
        } else {
            lastOpennedFeddLabel.text = ""
            lastOpennedFeddLabel.isUserInteractionEnabled = false
        }
    }
    
    private func setupView() {
        setupNameLabel()
        updateDateLabel()
        setupLastFeedTapGesture()
        setupStartButton()
    }
    
    private func setupNameLabel() {
        nameLabel.text = UIDevice.current.name
    }
    
    private func updateDateLabel() {
        viewmodel.date.bind { [weak self] date in
            self?.dateLabel.text = date
        }
    }
    
    private func setupLastFeedTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(lastFeedTapped(_:)))
        lastOpennedFeddLabel.isUserInteractionEnabled = true
        lastOpennedFeddLabel.addGestureRecognizer(tap)
    }
    
    private func setupStartButton() {
        startButton.setTitle("Start", for: [])
        startButton.setTitleColor(.white, for: [])
        
        startButton.layer.cornerRadius = startButton.frame.height / 2
        startButton.layer.borderWidth = 3
        startButton.layer.borderColor = UIColor.black.cgColor
        startButton.backgroundColor = UIColor.systemBlue
    }
    
    @objc private func lastFeedTapped(_ sender: UIGestureRecognizer) {
        performSegue(withIdentifier: "webViewController", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webViewController" {
            if let feed = lastOppenedFeed {
                if let webView = segue.destination as? WebViewController {
                    webView.urlString = feed.link
                }
            }
        }
    }
}
