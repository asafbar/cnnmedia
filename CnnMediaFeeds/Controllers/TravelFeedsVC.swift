//
//  TravelFeedsVC.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 22/10/2020.
//

import UIKit
import Alamofire

class TravelFeedsVC: UIViewController {

    let viewmodel = TravelFeedsViewmodel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        
        viewmodel.fetcTravelFeeds()
    }
    
    private func setupView() {
        setupTableview()
        bindViewmodel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.navigationItem.title = "Travel"
        
        viewmodel.activateAutoUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewmodel.deActivateAutoUpdate()
    }
    
    private func bindViewmodel() {
        viewmodel.travelFeeds.bind { [weak self] _ in
            self?.tableView.reloadData()
        }
    }
    
    private func setupTableview() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TravelCell.cellNib, forCellReuseIdentifier: TravelCell.cellID)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webViewController" {
            if let cell = sender as? TravelCell {
                if let webView = segue.destination as? WebViewController {
                    webView.urlString = cell.feed?.link
                }
            }
        }
    }
}

extension TravelFeedsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewmodel.travelFeeds.value.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TravelCell.cellID, for: indexPath) as? TravelCell else { return UITableViewCell() }
        
        let feedItem = viewmodel.travelFeeds.value.items[indexPath.row]
        cell.configure(feed: feedItem)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? TravelCell
        
        if let feed = cell?.feed {
            CnnMediaDefaults().setLastOppened(feed: feed)
        }
        
        performSegue(withIdentifier: "webViewController", sender: cell)
    }
}
