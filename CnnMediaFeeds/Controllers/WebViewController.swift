//
//  WebViewController.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var urlString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    private func setupView() {
        webView.navigationDelegate = self
        prepareUrl()
    }
    
    private func prepareUrl() {
        if let urlStr = urlString,
           let url = URL(string: urlStr) {
            webView.load(URLRequest(url: url))
        }
    }
}

extension WebViewController: WKNavigationDelegate {
    //TODO: Make more here if needed
}
