//
//  FeedItemModel.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class FeedItemModel: NSObject, Codable, NSCoding {
    
    var title: String = ""
    var link: String = ""
    var pubDate: Date?
    
    
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(link, forKey: "link")
        coder.encode(pubDate, forKey: "pubdate")
    }
    
    required convenience init?(coder: NSCoder) {
        let title = coder.decodeObject(forKey: "title") as! String
        let link = coder.decodeObject(forKey: "link") as! String
        let pubdate = coder.decodeObject(forKey: "pubdate") as? Date
        
        self.init(title: title,
                  link: link,
                  pubDate: pubdate)
    }
    
    init(title: String,
                  link: String,
                  pubDate: Date?) {
        self.title = title
        self.link = link
        self.pubDate = pubDate
    }
    
    override init() {
        super.init()
    }
}

