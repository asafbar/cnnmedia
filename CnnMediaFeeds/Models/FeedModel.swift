//
//  FeedModel.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class FeedModel: NSObject, Codable, NSCoding {
    
    var title: String = ""
    var link: String = ""
    var pubDate: Date?
    var lastBuildDate: Date?
    
    open var items: [FeedItemModel] = Array()
    
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(link, forKey: "link")
        coder.encode(pubDate, forKey: "pubdate")
        coder.encode(items, forKey: "items")
        coder.encode(lastBuildDate, forKey: "lastbuilddate")
    }
    
    required convenience init?(coder: NSCoder) {
        let title = coder.decodeObject(forKey: "title") as! String
        let link = coder.decodeObject(forKey: "link") as! String
        let pubdate = coder.decodeObject(forKey: "pubdate") as? Date
        let lastbuilddate = coder.decodeObject(forKey: "lastbuilddate") as? Date
        let items = coder.decodeObject(forKey: "items") as! [FeedItemModel]
        
        self.init(title: title,
                  link: link,
                  pubDate: pubdate,
                  lastBuildDate: lastbuilddate,
                  items: items)
    }
    
    init(title: String,
                  link: String,
                  pubDate: Date?,
                  lastBuildDate: Date?,
                  items: [FeedItemModel]) {
        self.title = title
        self.link = link
        self.pubDate = pubDate
        self.items = items
        self.lastBuildDate = lastBuildDate
    }
    
    override init() {
        super.init()
    }
}

