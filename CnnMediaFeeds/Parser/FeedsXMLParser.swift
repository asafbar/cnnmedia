//
//  FeedsXMLParser.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class FeedsXMLParser: XMLParserManager {
    
    private var elementName: String = ""
    var feed: FeedModel? = nil
    var currentString: String?
    var currentItem: FeedItemModel? = nil
    var parseError: NSError? = nil
    
    override init(data: Data) {
        super.init(data: data)
        
        self.parser?.delegate = self
    }

    func parse() -> (feed: FeedModel?, error: NSError?) {
        self.feed = FeedModel()
        self.currentItem = nil
        self.currentString = String()
        
        self.parser?.parse()
        return (feed: self.feed, error: self.parseError)
    }
}

extension FeedsXMLParser: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        self.currentString = String()
        
        if ((elementName == "item") || (elementName == "entry")) {
            self.currentItem = FeedItemModel()
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if let currentItem = self.currentItem {
            if ((elementName == "item") || (elementName == "entry")) {
                self.feed?.items.append(currentItem)
                return
            }
            
            if (elementName == "title") {
                currentItem.title = self.currentString ?? ""
            }
            
            if (elementName == "link") {
                currentItem.link = self.currentString ?? ""
            }
            
            if (elementName == "pubDate") {
                
                if let date = Date.dateFormatted(dateString: self.currentString) {
                    currentItem.pubDate = date
                }
            }
            
        } else {
            if (elementName == "title") {
                self.feed?.title = self.currentString ?? ""
            }
            
            if (elementName == "link") {
                self.feed?.link = self.currentString ?? ""
            }
            
            if (elementName == "pubDate") {
                
                if let date = Date.dateFormatted(dateString: self.currentString) {
                    feed?.pubDate = date
                }
            }
            
            if (elementName == "lastBuildDate") {
                
                if let date = Date.dateFormatted(dateString: self.currentString) {
                    feed?.lastBuildDate = date
                }
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        self.currentString?.append(string)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        self.parseError = parseError as NSError?
        self.parser?.abortParsing()
    }
}
