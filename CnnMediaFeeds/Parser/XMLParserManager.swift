//
//  XMLParserManager.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class XMLParserManager: NSObject {
    var parser: XMLParser?
    
    init(data: Data) {
        super.init()
        
        self.parser = XMLParser(data: data)
    }
    
    func parse() {
        parser?.parse()
    }
}
