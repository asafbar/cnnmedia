//
//  LastBuildDateXMLParser.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 24/10/2020.
//

import Foundation

class LastBuildDateXMLParser: XMLParserManager {
    private var elementName: String = ""
    var currentString: String?
    var lastBuilldDate: Date?
    
    var parseError: NSError? = nil
    
    override init(data: Data) {
        super.init(data: data)
        
        self.parser?.delegate = self
    }

    func parse() -> (lastBuilldDate: Date?, error: NSError?) {
        self.currentString = String()
        
        self.parser?.parse()
        return (lastBuilldDate: self.lastBuilldDate, error: self.parseError)
    }
}

extension LastBuildDateXMLParser: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "lastBuildDate" {
            self.currentString = String()
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if (elementName == "lastBuildDate") {
            if let date = Date.dateFormatted(dateString: self.currentString) {
                lastBuilldDate = date
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        self.currentString?.append(string)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        self.parseError = parseError as NSError?
        self.parser?.abortParsing()
    }
}
