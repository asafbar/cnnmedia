//
//  MixDataCell.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import UIKit

class MixDataCell: UITableViewCell, CellIdentifiable {

    enum MixCellColorType {
        case sport
        case entertainment
        case none
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var feed: FeedItemModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(feed: FeedItemModel, colorType: MixCellColorType? = nil) {
        self.feed = feed
        
        titleLabel.text = feed.title

        if let date = feed.pubDate {
            dateLabel.text =  date.stringFormatted()
        }
        
        setupColorType(colorType: colorType)
    }
    
    private func setupColorType(colorType: MixCellColorType?) {
        guard let colorType = colorType else { return }
        switch colorType {
        case .sport:
            print("Sport")
            contentView.backgroundColor = UIColor(red: 22 / 255, green: 0, blue: 100 / 255, alpha: 1)
            titleLabel.textColor = .white
            dateLabel.textColor = .white
        case .entertainment:
            print("entertainment")
            contentView.backgroundColor = UIColor(red: 133 / 255, green: 141 / 255, blue: 225 / 255, alpha: 1)
            titleLabel.textColor = .black
            dateLabel.textColor = .black
        default:
            contentView.backgroundColor = .white
            titleLabel.textColor = .black
            dateLabel.textColor = .black
        }
    }
}
