//
//  TravelCell.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import UIKit

class TravelCell: UITableViewCell, CellIdentifiable {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var feed: FeedItemModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(feed: FeedItemModel) {
        self.feed = feed
        
        titleLabel.text = feed.title

        if let date = feed.pubDate {
            dateLabel.text =  date.stringFormatted()
        }
    }
}
