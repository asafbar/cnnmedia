//
//  UserDefaultsKeys.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

protocol DefaultKeys {
    var name: String { get }
}

class UserDefaultsKeys {
    enum keys: DefaultKeys {
        case lastOppened
        
        var name: String {
            switch self {
            case .lastOppened : return "lastOppened"
            }
        }
    }
}
