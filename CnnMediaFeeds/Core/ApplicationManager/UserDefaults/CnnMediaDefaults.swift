//
//  CnnMediaDefaults.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

struct CnnMediaDefaults {
    private let defaults = UserDefaults.standard
    
    func setUserDefaults(value: Data, forKey key: UserDefaultsKeys.keys) {
        defaults.setValue(value, forKey: key.name)
        defaults.synchronize()
    }
    
    func getUserDefaultsDataValue(for key: UserDefaultsKeys.keys) -> Data? {
        return defaults.data(forKey: key.name)
    }
}

extension CnnMediaDefaults {
    func setLastOppened(feed: FeedItemModel) {
        do {
        let encodedData = try NSKeyedArchiver.archivedData(withRootObject: feed, requiringSecureCoding: false)
        
        setUserDefaults(value: encodedData, forKey: .lastOppened)
        } catch {
            print("Error saving feed in UserDefaults")
        }
    }
    
    func getLastOppenedFeed() -> FeedItemModel? {
        guard let archived = getUserDefaultsDataValue(for: .lastOppened) else { return nil }
            let feed = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(archived) as? FeedItemModel
            return feed
    }
}

