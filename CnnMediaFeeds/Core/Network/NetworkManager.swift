//
//  NetworkManager.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation
import Alamofire

class NetworkManager {
        
    func fetcData(url: URL, timeout: TimeInterval, completion: @escaping(Result<Data?, Error>) -> Void) {
        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = timeout

        AF.request(urlRequest).responseData { response in
            switch response.result {
            case .success(_):
                if let data = response.data {
                    completion(Result.success(data))
                } else {
                    completion(Result.success(nil))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}
