//
//  Config.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class Config: NSObject {
    
    static let updateSeconds = 5
    let updateInterval = TimeInterval(updateSeconds)
    let appDateUpdateInterval = TimeInterval(60)
    
    enum RssUrlString: String {
        case travel         = "http://rss.cnn.com/rss/edition_travel.rss"
        case worldSport     = "http://rss.cnn.com/rss/edition_sport.rss"
        case entertainment  = "http://rss.cnn.com/rss/edition_entertainment.rss"
    }
    
    var networkManager: NetworkManager?
    
    static let shared: Config = {
        let instance = Config()
        
        return instance
    }()
    
    override init() {
        networkManager = NetworkManager()
    }
    
}
