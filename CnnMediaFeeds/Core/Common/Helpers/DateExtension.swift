//
//  DateExtension.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

extension Date {
    func stringFormatted() -> String {
        let dateFormat = "E, d MMM yyyy HH:mm"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.locale = Locale.current
        
        return dateFormatter.string(from: self)
    }
    
    static func dateFormatted(dateString: String?) -> Date? {
        guard let dateStr = dateString else { return nil }
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
        return dateFormatter.date(from: dateStr)
    }
}
