//
//  Observable.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 22/10/2020.
//

import Foundation

class Observable<T> {
    typealias Listener = (T) -> ()
    
    var listener : Listener?
    
    var value : T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    deinit {
        listener = nil
    }
    
    func bind(_ listener: Listener?) {
        self.listener = listener
    }
}
