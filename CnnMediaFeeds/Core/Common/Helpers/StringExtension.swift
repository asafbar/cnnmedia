//
//  StringExtension.swift
//  CnnMediaFeeds
//
//  Created by Asaf Berko on 23/10/2020.
//

import Foundation

class DateExtension {
    
}

extension Date {
    func stringFormatted() -> String {
//        if self == nil {
//            return ""
//        }
        
        let dateFormat = "E, d MMM yyyy HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.locale = Locale.current
        
        return dateFormatter.string(from: self)
    }
}
