# README #

#CnnMedia

The CnnMedia is a simple app to fetch and mixed data from cnn rss media feeds and parse the data into a tableview

This app using the folowing pods:

- Alamofire 5.3


\May you need to update pods by running pod install from terminal

The app using Cnn media feeds

* [Cnn media feeds](https://edition.cnn.com/services/rss/)

Channels:

* [Entertainment](http://rss.cnn.com/rss/edition_entertainment.rss)

* [World Sport](http://rss.cnn.com/rss/edition_sport.rss)

* [Travel](http://rss.cnn.com/rss/edition_travel.rss)



### Application flow ###

First, Start page appear with the following information:
* Device name
* Date updated every minute
* Start button
* Its also contains a touchable label that stored from last oppened Feed url

Click on 'Start' button and go,
The app show a Tabbar with 2 tabs,
Each show the following:
* Travel Feeds
* Mixed - World sport and Entertainment

on click each feed item, Webview appear and show the selected feed like a native app
* When open feed url, this feed is stored to open later from Start page

## Start page
![Image description](screenshots/Startpage.png)

## Travel page
![Image description](screenshots/Travelpage.png)

## Mix Data page
![Image description](screenshots/MixDatapage1.png)
![Image description](screenshots/MixDatapage2.png)

## Webview page
![Image description](screenshots/Webviewpage.png)
